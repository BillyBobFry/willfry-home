import { Tech } from "./"

export type App = {
  name: string
  description: string
  url: string
  techs?: { name: Tech; logo?: any }[]
  backgroundURL?: string
}
