import React, { HTMLAttributes } from "react"

export interface BulbProps extends HTMLAttributes<HTMLOrSVGElement> {
  on: boolean
}

export const Bulb = ({ on, ...rootProps }: BulbProps) => (
  <svg
    {...rootProps}
    className="darkModeSwitch"
    width="100"
    height="127"
    viewBox="0 0 100 127"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <filter id="demo4">
      <feGaussianBlur stdDeviation="50" result="blur4" />

      <feSpecularLighting
        lightingColor="#9a9a9a"
        specularExponent="50"
        in="blur4"
      >
        <fePointLight x="50" y="60" z="200">
          {/* <feFlood
            flood-color="seagreen"
            flood-opacity="1"
            x="0"
            y="0"
            width="200"
            height="200"
          ></feFlood> */}
        </fePointLight>
      </feSpecularLighting>
      <feComposite
        in="SourceGraphic"
        in2="spec4"
        operator="arithmetic"
        k1="0"
        k2="1"
        k3="1"
        k4="0"
      />
    </filter>
    <filter id="demo5">
      <feGaussianBlur in="SourceGraphic" stdDeviation="2" result="blur5" />
      <feComposite
        in="SourceGraphic"
        in2="blur5"
        operator="arithmetic"
        k1="0"
        k2="3"
        k3="3"
        k4="0"
      />
    </filter>

    <circle
      cx="50"
      cy="50"
      r="50"
      fill={on ? "gold" : "#C4C4C4"}
      filter={on ? "url(#demo4)" : undefined}
    />
    <path d="M21 90H80V127H21V90Z" fill="#999999" />
    <line x1="50" y1="90" x2="50" y2="67" stroke="black" stroke-width="4" />
    <path
      d="M35.5 50C35.5 70.5 63 70.5 63 50"
      stroke="black"
      stroke-width="4"
    />
  </svg>
)
